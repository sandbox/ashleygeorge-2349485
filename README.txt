Twitter images allows you to pull and store image based Tweets to a
website based on a specific hashtag.

Imported Tweets are placed into an "Approved", "Pending" or "Rejected"
queue, which is manageable via an admin interface. Tweets are stored
as entities which allows site builders to easily choose how content is
used and displayed.

Although only a single Hashtag can be set at one time, each Tweet is
stored with it's associated hashtag. An example of where this may be
useful is when a campaign hashtag changes for different periods, yet
the site owner wishes to display both the current and previous
campaign Tweets in separate displays.