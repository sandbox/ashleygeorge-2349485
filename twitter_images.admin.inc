<?php
/**
 * @file.
 *
 * Defines the admin settings form.
 */
function twitter_images_admin_settings() {

  $form['hashtag']['twitter_images_hashtag'] = array(
    '#type' => 'textfield',
    '#title' => t('Search hashtag'),
    '#description' =>
      t('Enter a hashtag for importing Tweets.'),
    '#field_prefix' => '#',
    '#default_value' => variable_get('twitter_images_hashtag', 'drupal'),
  );

  $form['cron']['twitter_images_use_cron'] = array(
    '#type' => 'checkbox',
    '#title' => 'Automatically look for new Tweets',
    '#default_value' => variable_get('twitter_images_use_cron', FALSE)
  );

  $form['cron']['twitter_images_cron_max_frequency'] = array(
    '#type' => 'select',
    '#title' => t('Maximum frequency to check for new Tweets'),
    '#description' =>
      t('Select a maximum frequency for importing Tweets.
      NB. this depends on the !link.', array(
        '!link' => l(t('system-wide cron settings'), 'admin/config/system/cron'),
      )),
    '#options' => array(
      0 => t('weekly'),
      1 => t('daily'),
      2 => t('twice daily'),
      4 => t('every 6 hours'),
      8 => t('every 3 hours'),
      24 => t('hourly'),
    ),
    '#default_value' => variable_get('twitter_images_cron_max_frequency', 1),
    '#states' => array(
      "visible" => array(
        "input[name='twitter_images_use_cron']" => array("checked" => TRUE)),
    ),
  );

  $form = system_settings_form($form, TRUE);
  $form['#validate'][] = 'twitter_images_admin_settings_validate';

  return $form;
}

/**
 * Validate function
 *
 * Checks whether the frequency had changed, to update the next
 * execute date.
 */
function twitter_images_admin_settings_validate($form, &$form_state) {
  $stored_freq = variable_get('twitter_images_cron_max_frequency', 1);
  $form_freq = $form_state['values']['twitter_images_cron_max_frequency'];

  if ($stored_freq <> $form_freq) {
    variable_set('twitter_images_cron_next_execution', 0);
  }
}
