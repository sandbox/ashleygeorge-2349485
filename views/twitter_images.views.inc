<?php

/*
 * Implements hook_views_data
 */
function twitter_images_views_data() {
  $data = array();
  
  $data['twitter_images']['table']['group'] = t('Imported Twitter Images');
  $data['twitter_images']['table']['base'] = array(
    'title' => t('Imported Twitter Images'),
    'help' => t('Data imported from Twitter.'),
  );
  
  $data['twitter_images']['twitter_id'] = array(
    'title' => t('ID'),
    'help' => t('The tweet ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
 
  $data['twitter_images']['tweet_date'] = array(
    'title' => t('Tweet Date'),
    'help' => t('Date on Twitter.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  ); 
  
  $data['twitter_images']['import_date'] = array(
    'title' => t('Import Date'),
    'help' => t('Date imported to site.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  ); 
  
  $data['twitter_images']['image_url'] = array(
    'title' => t('Image'),
    'help' => t('The image on Twitter server.'),
    'field' => array(
      'handler' => 'twitter_images_handler_field_image_url',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  $data['twitter_images']['tweet_url'] = array(
    'title' => t('Tweet URL'),
    'help' => t('URL on twitter.com.'),
    'field' => array(
      'handler' => 'views_handler_field_url',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  $data['twitter_images']['status'] = array(
    'title' => t('Status'),
    'help' => t('Approval Status. 0 = pending, 1 = approved, 2 = ignored.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  $data['twitter_images']['tweet_body'] = array(
    'title' => t('Tweet body'),
    'help' => t('Content of the Tweet.'),
    'field' => array(
      'handler' => 'twitter_images_handler_field_utf8',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  $data['twitter_images']['twitter_hashtag'] = array(
    'title' => t('Hashtag'),
    'help' => t('Hashtag of the Tweet.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  $data['twitter_images']['twitter_user'] = array(
    'title' => t('Twitter User'),
    'help' => t('Handle of the posting Twitter user.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  return $data;
}