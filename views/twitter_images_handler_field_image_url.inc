<?php
/**
 * Custom views field handler to display the twitter image from Twitter's server.
 */
class twitter_images_handler_field_image_url extends views_handler_field {
  function render($values) {
    module_load_include('module', 'twitter_images', 'twitter_images');
    $img_url = $this->get_value($values);
    $options = array(
      'tweet_url' => $values->twitter_images_tweet_url,
      'tweet_body' => $values->twitter_images_tweet_body,
    );
    return _twitter_images_render_image($img_url, $options);
  }
}