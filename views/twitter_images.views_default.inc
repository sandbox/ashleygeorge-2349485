<?php
/*
 * Implements hook_views_default_views()
 */
function twitter_images_views_default_views() {
  $view = new view();
  $view->name = 'twitter_images';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'twitter_images';
  $view->human_name = 'Approved Tweets with Images';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Approved Tweets';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ prev';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Imported Twitter Images: Twitter User */
  $handler->display->display_options['fields']['twitter_user']['id'] = 'twitter_user';
  $handler->display->display_options['fields']['twitter_user']['table'] = 'twitter_images';
  $handler->display->display_options['fields']['twitter_user']['field'] = 'twitter_user';
  $handler->display->display_options['fields']['twitter_user']['label'] = '';
  $handler->display->display_options['fields']['twitter_user']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['twitter_user']['alter']['text'] = '@[twitter_user]';
  $handler->display->display_options['fields']['twitter_user']['element_type'] = 'h3';
  $handler->display->display_options['fields']['twitter_user']['element_label_colon'] = FALSE;
  /* Field: Imported Twitter Images: Image */
  $handler->display->display_options['fields']['image_url']['id'] = 'image_url';
  $handler->display->display_options['fields']['image_url']['table'] = 'twitter_images';
  $handler->display->display_options['fields']['image_url']['field'] = 'image_url';
  $handler->display->display_options['fields']['image_url']['label'] = '';
  $handler->display->display_options['fields']['image_url']['alter']['text'] = '<img src=\'[image_url]\'>';
  $handler->display->display_options['fields']['image_url']['element_label_colon'] = FALSE;
  /* Field: Imported Twitter Images: Tweet body */
  $handler->display->display_options['fields']['tweet_body']['id'] = 'tweet_body';
  $handler->display->display_options['fields']['tweet_body']['table'] = 'twitter_images';
  $handler->display->display_options['fields']['tweet_body']['field'] = 'tweet_body';
  $handler->display->display_options['fields']['tweet_body']['label'] = '';
  $handler->display->display_options['fields']['tweet_body']['element_label_colon'] = FALSE;
  /* Field: Imported Twitter Images: Tweet URL */
  $handler->display->display_options['fields']['tweet_url']['id'] = 'tweet_url';
  $handler->display->display_options['fields']['tweet_url']['table'] = 'twitter_images';
  $handler->display->display_options['fields']['tweet_url']['field'] = 'tweet_url';
  $handler->display->display_options['fields']['tweet_url']['exclude'] = TRUE;
  /* Sort criterion: Imported Twitter Images: Tweet Date */
  $handler->display->display_options['sorts']['tweet_date']['id'] = 'tweet_date';
  $handler->display->display_options['sorts']['tweet_date']['table'] = 'twitter_images';
  $handler->display->display_options['sorts']['tweet_date']['field'] = 'tweet_date';
  $handler->display->display_options['sorts']['tweet_date']['order'] = 'DESC';
  /* Filter criterion: Imported Twitter Images: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'twitter_images';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');

  $views[$view->name] = $view;
  return $views;
}