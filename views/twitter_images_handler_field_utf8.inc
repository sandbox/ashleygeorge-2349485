<?php
/**
 * Custom views field handler to display the twitter image from Twitter's server.
 */
class twitter_images_handler_field_utf8 extends views_handler_field {
  function render($values) {
    return utf8_decode($this->get_value($values));
  }
}