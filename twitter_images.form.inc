<?php
/**
 * @file
 *
 * Main moderation form for processing Tweets.
 *
 * This form is used for the pending, approved, ignored
 * tabs. These have slightly different options. The menu
 * system passes a variable $var depending on the url. This
 * variable corresponds to the 'state' of tweets show on the
 * page.
 */
function twitter_images_main_form($form, &$form_state, $var) {
  // Typecasts $var as an integer.
  $var = (int) $var;

  // Sets a title variable based on the origin.
  switch ($var) {
    case 0:
      $title = 'Pending Tweets';
      break;
    case 1:
      $title = 'Approved Tweets';
      break;
    case 2:
      $title = 'Ignored Tweets';
      break;
  }

  // Pulls in the data from database, according to the
  // required status.
  $data = db_select('twitter_images', 's')
    ->extend('PagerDefault')
    ->condition('status', $var)
    ->fields('s')
    ->orderBy('tweet_date', 'DESC')
    ->execute();

  // Get the row count from the dataset
  $row_count = $data->rowCount();

  // Display an update button on the pending tweet tab
  if ($var == 0) {
    $form['update tweets'] = array(
      '#type' => 'submit',
      '#value' => t('Check for new tweets'),
      '#submit' => array('twitter_images_main_form_submit_import'),
    );
  }

  // Display the main part of the for if there is any data
  if ($row_count > 0) {
    // Loop thru records and configure them for display
    // in the table.
    foreach ($data as $record) {
      // Skip this record is the data is bad.
      if(!isset($record->image_url) || empty($record->image_url) ||
          !isset($record->tweet_url) || empty($record->tweet_url)){
        continue;
      }
      
      $img_url = $record->image_url;
      $tweet_body = $record->tweet_body;
      
      $image_vars = array(
        'tweet_url' => $record->tweet_url,
        'tweet_body' => $tweet_body,
      );

      $image_link = _twitter_images_render_image($img_url, $image_vars);
      
      // Set option values in table.
      $options[$record->twitter_id] = array(
        'the_id' => $record->twitter_id,
        'the_tweet_date' => date("Y-m-d H:i:s", $record->tweet_date),
        'the_tweet' => $tweet_body,
        'the_image' => $image_link,
        'the_hashtag' => $record->twitter_hashtag,
      );
    }

    $form['action'] = array(
      '#type' => 'select',
      '#title' => t('Apply action to selected tweets'),
    );
    // Don't give an option to update a tweet to its current status
    if ($var <> 0) {
      $form['action']['#options'][0] = t('Reset to pending');
    }
    if ($var <> 1) {
      $form['action']['#options'][1] = t('Approve');
    }
    if ($var <> 2) {
      $form['action']['#options'][2] = t('Ignore');
    }
    // Only display a delete option for currently ignored tweets.
    if ($var == 2) {
      $form['action']['#options'][3] = t('Delete');
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit Action'),
      '#submit' => array('twitter_images_main_form_submit_update'),
      '#validate' => array('twitter_images_main_form_validate_update'),
    );

    $header = array(
      'the_image' => t('Image'),
      'the_tweet' => t('Tweet Text'),
      'the_tweet_date' => t('Tweet Date'),
      'the_id' => t('ID'),
      'the_hashtag' => t('Hashtag'),
    );

    $form['table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#multiple' => TRUE,
    );

    $form['pager'] = array(
      '#markup' => theme('pager')
    );

  }
  else{
    $form['no_data'] = array(
      '#markup' => t('There are currently no ' . $title .
      '. Configure how Tweets are imported on the !link.', array(
        '!link' => l(t('config page'), 'admin/config/services/twitter_images/settings'),
      )),
    );
  }

  return $form;
}

/**
 * Performs the queries to update the Tweets
 */
function twitter_images_update_state($state, $ids) {

  if ($state == 3) {
    $count = db_delete('twitter_images')
      ->condition('twitter_id', $ids, 'IN')
      ->execute();
  }
  else{
    $count = db_update('twitter_images')
      ->fields(array(
        'status' => $state,
      ))
      ->condition('twitter_id', $ids, 'IN')
      ->execute();
  }

  $tweets = ($count > 1) ? 'tweets' : 'tweet';

  switch ($state) {
    case 0:
      drupal_set_message(t("@count @tweets set to 'pending'.",
        array(
          '@count' => $count, 
          '@tweets' => $tweets,
        )
      ));
      break;
    case 1:
      drupal_set_message(t("@count @tweets set to 'approved'.",
        array(
          '@count' => $count, 
          '@tweets' => $tweets,
        )
      ));    
      break;
    case 2:
      drupal_set_message(t("@count @tweets set to 'ignored'.",
        array(
          '@count' => $count, 
          '@tweets' => $tweets,
        )
      ));    
      break;                         
    case 3:
      drupal_set_message(t("@count @tweets set to deleted.",
        array(
          '@count' => $count, 
          '@tweets' => $tweets,
        )
      ));    
      break; 
  }
}

/**
 * Submit handler for update button
 */
function twitter_images_main_form_submit_update($form, &$form_state) {
  $ids = $form['table']['#value'];
  $action = $form['action']['#value'];
  twitter_images_update_state($action, $ids);
}

/*
 * Validate handler for update button
 */
function twitter_images_main_form_validate_update($form, &$form_state) {
  $values = $form['table']['#value'];
  if (empty($values)) {
    form_set_error('table', t('Please select one or more tweets.'));
  }
}

/*
 * Submit handler for import button
 */
function twitter_images_main_form_submit_import($form, &$form_state) {
  module_load_include('inc', 'twitter_images', 'twitter_images.twitter');  
  import_twitter_data();
}