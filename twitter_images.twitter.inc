<?php

/**
 * @file
 * Gets the required Tweet data.
 * 
 */
function import_twitter_data() {

  // Query db to find highest tweet id.
  $query = db_select('twitter_images');
  $query->addExpression('MAX(twitter_id)');
  $since_id = $query->execute()->fetchField();

  // Prepare connection to Twitter search API.
  module_load_include('inc', 'twitter', 'twitter');
  $twitter_object = twitter_connect();

  // Get Twitter query from stored variable,
  // with the max Tweet ID as a parameter.
  $hashtag = variable_get('twitter_images_hashtag', 'drupal');

  $twitter_query_q = '#' . $hashtag . ' filter:images -rt';
  $twitter_query = array(
    'q' => $twitter_query_q,
    'count' => 50,
    'include_entities' => TRUE,
    'since_id' => $since_id,
  );

  // Execute API call.
  $tweets = json_decode($twitter_object->auth_request('https://api.twitter.com/1.1/search/tweets.json', $twitter_query, 'GET'));

  // Loop through the results and append to database.
  foreach ($tweets->statuses as $tweet) {
    add_tweet_to_database($tweet, $hashtag);
  }

  // Set count of imported tweets.
  $count = count($tweets->statuses);

  // Display feedback on import.
  switch ($count) {
    case 0:
      drupal_set_message(t('No new tweets to import'));
      break;
    case 1:
      drupal_set_message(t('1 tweet imported'));
      break;
    default:
      drupal_set_message(t('@count tweets imported',
        array(
          '@count' => $count,
        )
      ));
      break;
  }
}

/**
 * Create a tweet entity from a given id.
 */
function add_tweet_to_database($tweet, $hashtag) {

  // Prepare record object.
  $tweet_date = new DateTime($tweet->created_at);

  // Add the fields to the entity and save.
  $entity = entity_create('twitter_images', array());
  $entity->twitter_id = $tweet->id;
  $entity->status = 0;
  $entity->tweet_date = $tweet_date->getTimestamp();
  $entity->import_date = time();
  $entity->image_url = $tweet->entities->media[0]->media_url;
  $entity->tweet_url = $tweet->entities->media[0]->url;
  $entity->twitter_user = $tweet->user->screen_name;
  $entity->tweet_body = utf8_encode($tweet->text);
  $entity->twitter_hashtag = $hashtag;
  $entity->save();
}
